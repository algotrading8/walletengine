package com.algotrading.walletengine.users;

import javax.persistence.*;

@Entity
@Table(name = "Walletdetail")
public class WalletDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column( name = "DETAIL_ID")
    private Integer detailId;

    @Column(name = "WALLET_ID")
    private String walletId;

    @Column(name = "SYMBOL")
    private String symbol;

    @Column(name = "ORIGIN_SOURCE")
    private String originSource;

    @Column(name = "QTY")
    private Double qty;

    @Column(name = "BUY_PRICE")
    private Double buyPrice;

    public Integer getDetailId() {
        return detailId;
    }

    public void setDetailId(Integer detailId) {
        this.detailId = detailId;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getOriginSource() {
        return originSource;
    }

    public void setOriginSource(String originSource) {
        this.originSource = originSource;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Double buyPrice) {
        this.buyPrice = buyPrice;
    }

    @Override
    public String toString() {
        return "WalletDetail{" +
                "walletId='" + walletId + '\'' +
                ", symbol='" + symbol + '\'' +
                ", originSource='" + originSource + '\'' +
                ", qty=" + qty +
                ", buyPrice=" + buyPrice +
                '}';
    }
}
