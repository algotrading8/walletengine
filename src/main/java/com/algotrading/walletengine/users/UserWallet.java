package com.algotrading.walletengine.users;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Userwallet")
public class UserWallet {

    @Column(name = "USER_ID")
    private int userId;

    @Id
    @Column(name = "WALLET_ID")
    private String userCode;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    @Override
    public String toString() {
        return "UserWallet{" +
                "userId=" + userId +
                ", userCode='" + userCode + '\'' +
                '}';
    }
}
