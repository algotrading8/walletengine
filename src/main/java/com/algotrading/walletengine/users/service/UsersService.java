package com.algotrading.walletengine.users.service;

import com.algotrading.walletengine.users.Users;
import com.algotrading.walletengine.users.WalletDetail;
import com.algotrading.walletengine.users.WalletTransaction;

import java.util.List;

public interface UsersService {

    List<Users> findAll();

    Users getUserById(int id);

    Users getUserByCode(String userCode);

    List<WalletDetail> getFullWalletById(String id);

    List<WalletTransaction> getFullWalletTransaction(String id);

    int insertWalletTransaction(WalletTransaction transaction);

    int insertWalletDetail(WalletDetail walletDetail);

    List<WalletDetail> getSymbolWalletById(String id, String symbol);
}
