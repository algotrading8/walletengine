package com.algotrading.walletengine.users.service;

import com.algotrading.walletengine.users.Users;
import com.algotrading.walletengine.users.WalletDetail;
import com.algotrading.walletengine.users.WalletTransaction;
import com.algotrading.walletengine.users.repository.UsersRepository;
import com.algotrading.walletengine.users.repository.WalletTransactionRepository;
import com.algotrading.walletengine.users.repository.WalletUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private WalletUserRepository walletUserRepository;
    @Autowired
    private WalletTransactionRepository walletTransactionRepository;

    @Override
    public List<Users> findAll() {
        return usersRepository.findAll();
    }

    @Override
    public Users getUserById(int id) {
        return usersRepository.getUserById(id);
    }

    @Override
    public Users getUserByCode(String userCode) {
        return usersRepository.getUserByCode(userCode);
    }

    @Override
    public List<WalletDetail> getFullWalletById(String id) {
        return walletUserRepository.getFullWalletById(id);
    }

    public int insertWalletTransaction(WalletTransaction transaction) {
        int inserted = 0;
        /*
        TODO: ahora mismo solo se piensa que se cambia TODO lo que tengas de esa divisa. Suponer también el caso de cambio parcial
        */
        int update = walletUserRepository.updateWalletUser(transaction.getSymbolTo(),
                        transaction.getQty(),
                        transaction.getChangePrice(),
                        transaction.getWalletId(),
                        transaction.getBuyPrice(),
                        transaction.getSymbolFrom());


        inserted = walletTransactionRepository.insertWalletTransaction(transaction.getWalletId(),
                transaction.getSymbolFrom(),
                transaction.getSymbolTo(),
                transaction.getBuyPrice(),
                transaction.getChangePrice(),
                transaction.getQty(),
                transaction.getPriceTo(),
                transaction.getQtyTo());
        
        return inserted;
    }

    @Override
    public int insertWalletDetail(WalletDetail walletDetail) {
        try{
            walletUserRepository.insertWalletDetail(walletDetail.getWalletId(),
                    walletDetail.getSymbol(),
                    walletDetail.getSymbol(),
                    walletDetail.getQty(),
                    walletDetail.getBuyPrice());
        }catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public List<WalletDetail> getSymbolWalletById(String id, String symbol) {
        return walletUserRepository.getSymbolWalletById(id, symbol);
    }

    public List<WalletTransaction> getFullWalletTransaction(String id) {
        return walletTransactionRepository.finAllByWalletId(id);
    }
}
