package com.algotrading.walletengine.users.repository;

import com.algotrading.walletengine.users.WalletDetail;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface WalletUserRepository extends CrudRepository<WalletDetail, Integer> {

    @Query(value = "SELECT * FROM WalletDetail WHERE wallet_id IN (SELECT wallet_id FROM Userwallet WHERE user_id = :id) ", nativeQuery = true)
    List<WalletDetail> getFullWalletById(@Param(value="id") String id);

    @Transactional
    @Modifying
    @Query(value = "UPDATE WalletDetail SET SYMBOL = :symbolTo, QTY = :qty, BUY_PRICE = :changePrice " +
            " WHERE WALLET_ID = :walletId AND BUY_PRICE = :buyPrice AND symbol = :symbolFrom ", nativeQuery = true)
    int updateWalletUser(@Param(value="symbolTo") String symbolTo,
                         @Param(value="qty") Double qty,
                         @Param(value="changePrice") Double changePrice,
                         @Param(value="walletId") String walletId,
                         @Param(value="buyPrice") Double buyPrice,
                         @Param(value="symbolFrom") String symbolFrom);

    @Transactional
    @Modifying
    @Query(value = "Insert Into WalletDetail(wallet_id, symbol, origin_source, qty, buy_price) " +
            "values (:walletId, :symbol, :source, :qty, :price)", nativeQuery = true)
    int insertWalletDetail(@Param(value="walletId") String walletId,
                           @Param(value="symbol") String symbol,
                           @Param(value="source") String source,
                         @Param(value="qty") Double qty,
                         @Param(value="price") Double price);

    @Query(value = "SELECT * FROM WalletDetail WHERE wallet_id IN (SELECT wallet_id FROM Userwallet WHERE user_id = :id) " +
            " AND symbol = :symbol", nativeQuery = true)
    List<WalletDetail> getSymbolWalletById(@Param(value="id") String id,
                                           @Param(value="symbol") String symbol);
}
