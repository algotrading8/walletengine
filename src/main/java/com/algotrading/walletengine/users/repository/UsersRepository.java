package com.algotrading.walletengine.users.repository;

import com.algotrading.walletengine.users.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends CrudRepository<Users, Integer> {

    List<Users> findAll();

    @Query(value = "SELECT * FROM Users WHERE user_id = :userId ", nativeQuery = true)
    Users getUserById(@Param(value="userId") int userId);

    @Query(value = "SELECT * FROM Users WHERE user_code = :userCode ", nativeQuery = true)
    Users getUserByCode(@Param(value="userCode") String userCode);
}
