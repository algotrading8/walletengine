package com.algotrading.walletengine.users.repository;

import com.algotrading.walletengine.users.WalletTransaction;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface WalletTransactionRepository  extends CrudRepository<WalletTransaction, Integer> {

    @Transactional
    @Modifying
    @Query(value="INSERT INTO WalletTransaction (wallet_id, symbol_from, symbol_to, buy_price, change_price, qty, price_to, qty_to) " +
            "VALUES (:walletId, :symbolFrom, :symbolTo, :buyPrice, :changePrice, :qty, :priceTo, :qtyTo)", nativeQuery = true)
    int insertWalletTransaction(@Param(value="walletId") String walletId,
                                @Param(value="symbolFrom") String symbolFrom,
                                @Param(value="symbolTo") String symbolTo,
                                @Param(value="buyPrice") Double buyPrice,
                                @Param(value="changePrice") Double changePrice,
                                @Param(value="qty") Double qty,
                                @Param(value="priceTo") Double priceTo,
                                @Param(value="qtyTo") Double qtyTo);

    @Query(value = "SELECT * FROM WalletTransaction WHERE wallet_id IN (SELECT wallet_id FROM Userwallet WHERE user_id = :userId)", nativeQuery = true)
    List<WalletTransaction> finAllByWalletId(@Param(value="userId") String userId);
}
