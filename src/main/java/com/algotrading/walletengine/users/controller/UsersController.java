package com.algotrading.walletengine.users.controller;

import com.algotrading.walletengine.users.Users;
import com.algotrading.walletengine.users.WalletDetail;
import com.algotrading.walletengine.users.WalletTransaction;
import com.algotrading.walletengine.users.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping({"/users"})
public class UsersController {

    @Autowired
    UsersService usersService;

    @RequestMapping(value="/findAll", method = RequestMethod.GET)
    public List<Users> getResponsable() {
        List<Users> personas = usersService.findAll();
        return usersService.findAll();
    }

    @RequestMapping(value="/getUserById/{id}", method = RequestMethod.GET)
    public Users getUserById(@PathVariable(value="id") int id) {
        return usersService.getUserById(id);
    }

    @RequestMapping(value="/getUserByCode/{code}", method = RequestMethod.GET)
    public Users getUserByCode(@PathVariable(value="code") String code) {
        return usersService.getUserByCode(code);
    }

    @RequestMapping(value="/getFullWalletById/{id}", method = RequestMethod.GET)
    public List<WalletDetail> getFullWalletById(@PathVariable(value="id") String id) {
        return usersService.getFullWalletById(id);
    }

    @RequestMapping(value="/insertWalletTransaction", method = RequestMethod.POST)
    public int insertWalletTransaction(@RequestBody WalletTransaction transaction){
        return usersService.insertWalletTransaction(transaction);
    }

    @RequestMapping(value="/insertWalletDetail", method = RequestMethod.POST)
    public int insertWalletDetail(@RequestBody WalletDetail walletDetail){
        return usersService.insertWalletDetail(walletDetail);
    }

    @RequestMapping(value="/getFullWalletTransaction/{idUser}", method = RequestMethod.GET)
    public List<WalletTransaction> getFullWalletTransaction(@PathVariable(value="idUser") String id) {
        return usersService.getFullWalletTransaction(id);
    }

    @RequestMapping(value="/getSymbolWalletById/{symbol}/{id}", method = RequestMethod.GET)
    public List<WalletDetail> getSymbolWalletById(@PathVariable(value="id") String id,
                                                @PathVariable(value="symbol") String symbol) {
        return usersService.getSymbolWalletById(id, symbol);
    }
}
