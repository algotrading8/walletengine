package com.algotrading.walletengine.users;

import javax.persistence.*;

@Entity
@Table(name = "Wallettransaction")
public class WalletTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column( name = "TRANSACTION_ID")
    private Integer transactionId;

    @Column(name = "WALLET_ID")
    private String walletId;

    @Column(name = "SYMBOL_FROM")
    private String symbolFrom;

    @Column(name = "SYMBOL_TO")
    private String symbolTo;

    @Column(name = "BUY_PRICE")
    private Double buyPrice;

    @Column(name = "CHANGE_PRICE")
    private Double changePrice;

    @Column(name = "PRICE_TO")
    private Double priceTo;

    @Column(name = "QTY_TO")
    private Double qtyTo;

    @Column(name = "QTY")
    private Double qty;

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getSymbolFrom() {
        return symbolFrom;
    }

    public void setSymbolFrom(String symbolFrom) {
        this.symbolFrom = symbolFrom;
    }

    public String getSymbolTo() {
        return symbolTo;
    }

    public void setSymbolTo(String symbolTo) {
        this.symbolTo = symbolTo;
    }

    public Double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Double getChangePrice() {
        return changePrice;
    }

    public void setChangePrice(Double changePrice) {
        this.changePrice = changePrice;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getPriceTo() {
        return priceTo;
    }

    public void setPriceTo(Double priceTo) {
        this.priceTo = priceTo;
    }

    public Double getQtyTo() {
        return qtyTo;
    }

    public void setQtyTo(Double qtyTo) {
        this.qtyTo = qtyTo;
    }

    @Override
    public String toString() {
        return "WalletTransaction{" +
                "transactionId=" + transactionId +
                ", walletId='" + walletId + '\'' +
                ", symbolFrom='" + symbolFrom + '\'' +
                ", symbolTo='" + symbolTo + '\'' +
                ", buyPrice=" + buyPrice +
                ", changePrice=" + changePrice +
                ", priceTo=" + priceTo +
                ", qtyTo=" + qtyTo +
                ", qty=" + qty +
                '}';
    }
}
