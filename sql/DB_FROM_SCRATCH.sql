#Creating the DB
CREATE DATABASE walletengine;
USE walletengine;

#Creating tables needed
CREATE TABLE Users (user_code CHAR(50),
					user_id INT,
                    user_name CHAR(255),
                    PRIMARY KEY(user_id)
                    );

CREATE TABLE UserWallet(user_id INT,
						wallet_id INT,
                        PRIMARY KEY(wallet_id),
                        FOREIGN KEY(user_id) REFERENCES Users(user_id)
                        );
                        
CREATE TABLE WalletDetail(detail_id INT NOT NULL AUTO_INCREMENT,
						wallet_id INT,
						symbol CHAR(12),
                        origin_source CHAR(50),
                        qty	DOUBLE,
                        buy_price DOUBLE,
                        PRIMARY KEY(detail_id),
                        FOREIGN KEY(wallet_id) REFERENCES UserWallet(wallet_id)
                        );
                        
CREATE TABLE WalletTransaction(transaction_id INT NOT NULL AUTO_INCREMENT,
							   wallet_id INT,
							   symbol_from CHAR(12),
                               symbol_to CHAR(12),
                               buy_price DOUBLE,
                               change_price DOUBLE,
                               qty DOUBLE,
                               price_to DOUBLE,
                               qty_to DOUBLE,
                               PRIMARY KEY(transaction_id),
                               FOREIGN KEY(wallet_id) REFERENCES UserWallet(wallet_id));

COMMIT;